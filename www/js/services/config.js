serviceModule.service('appConfig', ['$http', function($http) {
	
	var _config = null;
	var _url = 'http://demo6546857.mockable.io/config';


	this.get = function(callback) {
		if (_config == null) {
			$http.get(_url).then(function(resp) {
				console.log('Config initialized by API GET /config');

				_config = resp.data;
				if (typeof callback == 'function') {
					callback(_config);
				}
			}, function(err) {
				console.error('ERR', err);
				// err.status will contain the status code
			});
		} else if (typeof callback == 'function') {
			console.log('Config already initialized');
			callback(_config);
		}
	};

}]);