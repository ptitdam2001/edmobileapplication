controllerModule.controller('prehomeCtrl',
	['$scope', '$http', '$sce', 'appConfig', '$timeout', '$ionicLoading', '$state', 'EDRestwebservice', '$ionicPopup',
	function($scope, $http, $sce, appConfig, $timeout, $ionicLoading, $state, EDRestwebservice, $ionicPopup) {

		$scope.loaderPct = 0;
		$scope.loading = false;

		//get application informations
		appConfig.get(function(config) {
			$scope.app_description = config.description;
			$scope.app_slogan = $sce.trustAsHtml(config.slogan);

		});

		//Erreur popup
		$scope.showAlert = function() {
			var alertPopup = $ionicPopup.alert({
				title: 'Problème',
				template: 'Impossible de charger les Données',
				cssClass: 'error-popup',
				okType: 'button-dark'
			});
			alertPopup.then(function(res) {
				console.log('error on question and answer loading');
				$state.go('prehome');
			});
		};

		//add function on click
		$scope.loadData = function() {
			$scope.loading = true;

			//init loader
			$ionicLoading.show({
				templateUrl: 'templates/loader.html',
				scope: $scope
			});

			//init data into local database
			$timeout(function () {
				EDRestwebservice.getAllQuestionsAndAnswers(true, function(data) {
					$scope.loaderPct = 50;

					//load Products
					EDRestwebservice.getAllProducts(function(){
						$scope.loaderPct = 100;
						$ionicLoading.hide();

						//redirect to first question page
						$state.transitionTo('question');

					}, function() {
						//TODO on loading error
					});
				}, function(error, status, headers) {
					// An alert dialog
					
					$scope.showAlert();
				});
			}, 3000);
		};
	}
]);