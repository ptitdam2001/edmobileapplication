controllerModule.controller('ProductsCtrl', ['$scope', '$state', '$stateParams', 'EDRestwebservice', '$ionicSlideBoxDelegate', '$ionicHistory',
	function($scope, $state, $stateParams, EDRestwebservice, $ionicSlideBoxDelegate, $ionicHistory) {
		$scope.answerId = $stateParams.aid;
		$scope.viewMode = 'carousel';

		//init filter
		$scope.filters = {
			'filter_spiritueux': '',
			'filter_aroma': '',
			'filter_price': ''
		};

		$scope.switchViewList = function() {
			//console.debug("switchViewList")
			$scope.viewMode = $scope.viewMode == 'carousel' ? 'list' : 'carousel';
		}

		/**
		 * Carousel Configuration
		 */
		$scope.slideHasChanged = function(index) {
			//alert('slideHasChanged $index=' + index);
			//$scope.slideIndex = index;
			$ionicSlideBoxDelegate.next();
		}

		$scope.prevProduct = function() {
			$ionicSlideBoxDelegate.previous();
			//$scope.slideIndex = $ionicSlideBoxDelegate.currentIndex();
			console.log("previous " + $scope.slideIndex);
		}

		$scope.nextProduct = function() {
			$ionicSlideBoxDelegate.next();
			//$scope.slideIndex = $ionicSlideBoxDelegate.currentIndex();
			console.log("next " + $scope.slideIndex);
		}


		/**
		 * Back button configuration
		 */
		$scope.goBack = function() {
			if ($scope.answerId) {
				EDRestwebservice.getQuestionbyAnswerId($scope.answerId, function(question) {
					$state.go('question', {'qid': question.id});
				});
			} else {
				$state.go('question');
			}

			$ionicHistory.goBack();
		}

		/**
		 * Product link configuration
		 */
		$scope.gotoProduct = function(productid) {
			$state.transitionTo('product', {'pid': productid}, {'location' : true });
		}

		/**
		 * Page Configuration
		 */
		EDRestwebservice.getProductsByAnswer($scope.answerId).then(function(products) {
			$scope.rawProducts = products;
			$scope.products = products;

			$scope.$on('$ionicView.beforeEnter', function() {
				$ionicSlideBoxDelegate.update();

			});
			$scope.slideIndex = $ionicSlideBoxDelegate.currentIndex();

			//init form filter values
			$scope.list_spiritueux = [];
			angular.forEach($scope.rawProducts, function(pdt) {
				if (typeof(pdt.spiritueux) == 'string') {
					$scope.list_spiritueux.push(pdt.spiritueux);
				} else {
					angular.forEach(pdt.spiritueux, function(item) {
						$scope.list_spiritueux.push(item);
					});
				}
			});

			$scope.list_aromas = [];
			angular.forEach($scope.rawProducts, function(pdt) {
				if (typeof(pdt.aromes) == 'string') {
					$scope.list_aromas.push(pdt.aromes);
				} else {
					angular.forEach(pdt.aromes, function(item) {
						$scope.list_aromas.push(item);
					})
				}
				
			});

			//init image height
			$scope.imageheight = Math.round(document.getElementsByClassName('product_carousel')[0].clientHeight * 0.4);
		});
	}
]);