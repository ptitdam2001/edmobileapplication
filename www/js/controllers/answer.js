controllerModule.controller('AnswerCtrl', ['$scope', '$stateParams', '$state','EDRestwebservice', 
	function($scope, $stateParams, $state, EDRestwebservice) {

		$scope.qid = angular.isDefined($stateParams.qid) ? parseInt($stateParams.qid) : null;
	    $scope.aid = angular.isDefined($stateParams.aid) ? parseInt($stateParams.aid) : null;

	    //If there is no more question, we redirect to product list
		if ($scope.aid != null) {
			EDRestwebservice.getAnswer($scope.aid).then(function(currentAnswer) {
				if (currentAnswer.next_qid == null) {
					//No more question
					$state.transitionTo('products', {'aid' : $scope.aid});
				} else {

					//next question
					$state.go('question', {'qid': currentAnswer.next_qid});
				}
			})
		}
	}
]);