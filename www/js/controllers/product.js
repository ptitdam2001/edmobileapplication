controllerModule.controller('ProductCtrl', ['$scope', '$stateParams', '$state','EDRestwebservice','$ionicHistory', 
	function($scope, $stateParams, $state, EDRestwebservice, $ionicHistory) {
		$scope.productId = $stateParams.pid;

		$scope.goBack = function() {
			$ionicHistory.goBack();
		};

		EDRestwebservice.getProduct($scope.productId).then(function(product) {
			$scope.product = product;
		});
	}
]);