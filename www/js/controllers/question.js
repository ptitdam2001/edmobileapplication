
controllerModule.controller('QuestionCtrl', ['$scope', '$stateParams', '$state','EDRestwebservice', 
	function($scope, $stateParams, $state, EDRestwebservice) {

		$scope.questionId = angular.isDefined($stateParams.qid) ? parseInt($stateParams.qid) : null;

		if ($stateParams.qid) {
			EDRestwebservice.getQuestionbyId($scope.questionId, function(currentQuestion) {
				$scope.current_question = currentQuestion;
			});
		} else {
			EDRestwebservice.getRootQuestion(function(currentQuestion) {
				$scope.current_question = currentQuestion;
			});
		}

		//init back
		$scope.goBack = function() {
			if (angular.isDefined($scope.current_question.parent) && parseInt($scope.current_question.parent) > 0) {
				EDRestwebservice.getQuestionbyId($scope.current_question.parent, function(parent) {
					$state.transitionTo('question', parent.parent > 0 ? {'qid' : parent.id} : {}, {'reload': true});
				});
			} else {
				$state.transitionTo('question', {}, {'reload': true});
			}
		};
	}
]);