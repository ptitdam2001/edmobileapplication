// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('espritdegustation', ['ionic', 'ed.controllers', 'ed.services', 'ed.filters'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/')

  $stateProvider.state('prehome', {
    url: '/',
    templateUrl: 'templates/prehome.html',
    controller: 'prehomeCtrl'
  })
  .state('question', {
    url: '/question/:qid',
    templateUrl: 'templates/question.html',
    controller: 'QuestionCtrl'
  })
  .state('answer', {
      cache: false,
      url: '/answer/:qid/:aid',
      controller: 'AnswerCtrl'
  })
  .state('products', {
    cache: false,
    url: '/result/:aid/products',
    templateUrl: 'templates/products.html',
    controller: 'ProductsCtrl'
  })
  .state('product', {
    cache: false,
    url: '/product/:pid',
    templateUrl: 'templates/product.html',
    controller: 'ProductCtrl'
  });

})
